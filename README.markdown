# KDL~XSLT

This repository contains X·S·L·T transformations geared at working with the
  [K·D·L Document Language][KDL].

The general idea is that tooling for transforming dataful X·M·L into
  human‐readable X·M·L (i·e X·H·T·M·L) is straightforward and wel·established,
  so the problem of transforming K·D·L into human‐readable X·M·L can be reduced
  to the problem of transforming K·D·L into dataful X·M·L.
The [K·D·L Vocabulary][1024/KDL] provides a means of achieving this
  (serializing K·D·L as R·D·F/X·M·L).
X·S·L·T was chosen as the implementation language because it makes constructing
  X·M·L easy (even tho it makes parsing hard).

The goal is to have tools which work with `libxml` and `libxslt`, which are
  already available on many computers, so the transformations here·in utilize
  [X·S·L·T 1.0][XSLT10] (+ [E·X·S·L·T][EXSLT]) and not any later version.

## Usage

`KDL2XML.xslt`, when applied to an X·M·L document, will transform any
  `<html:script>` elements with a `@type` of `text/kdl`, `text/x-kdl` or
  `text/x.kdl` into either `<kdl:Document>` elements (if the text contents are
  a valid K·D·L document) or `<kdl2xml:TokenizationError>` or
  `<kdl2xml:ParseError>` elements (if they are not).
It will also do this for elements with an `rdf:parseType` of `KDL`.
As a third option, you can import the stylesheet and call `parse` with a
  `source` param of your choosing.

The following shell commands can be used to transform a source K·D·L document
  `source.kdl` into X·M·L :⁠—

```sh
printf '%s\n' "$(
  printf '%b' '<?xml version=\042\061.0\042?>\n<script xmlns=\0042http://www.w3.org/1999/xhtml\0042 type=\0042text/kdl\0042><![CDATA['
  cat source.kdl | \
    tr '\000\013\014' '\032\011\012' | \
    sed $(
      printf '%s%s%s%s%s%b' \
      's/]]>/]]]]><!\[CDATA\[>/g;' \
      's/\xEF\xBF\xBE/�/g;s/\xEF\xBF\xBF/�/g;' \
      '$!s/\r$//g;s/\r/\n/g;' \
      '$!s/\xC2\x85$//g;s/\xC2\x85/\n/g;' \
      's/\xE2\x80\xA8/\n/g;' \
      's/[\0001-\0010]/�/g;s/[\0016-\0037]/�/g'
    )
  printf '%s' ']]></script>'
)" | xsltproc KDL2XML.xslt -
```

The provided `GNUmakefile` provides a shortcut for this:
  `make kdl2xml SOURCE=source.kdl`.

`KDL2XML.xslt` is a recursive parser and so may be subject to stack depth
  limitations; these can be adjusted in `xsltproc` via the `--maxdepth` option.
It is probably not suitable for transforming K·D·L documents more than a few
  kilobytes in length (but the limits haven’t been properly tested).
Creating a non‐recursive parser with `exslstr:tokenize()` is probably
  hypothetically possible but computationally and conceptually significantly
  more difficult.

## Current Status

Developed and maintained by [kibigo!](https://gitlab.com/kibigo) at U·C Santa
  Barbara as a professional development exercise.

Currently, these transformations are targeting the first version of the K·D·L
  specification.
Support for K·D·L 2.0 <s>or some other variant</s> may come at a future date.

## Licence (M·I·T)

```txt
Copyright © 2024 The Regents of the University of California.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

[1024/KDL]: <https://ns.1024.gdn/KDL/>
[EXSLT]: <https://web.archive.org/web/20211225232437/http://exslt.org/>
[KDL]: <https://kdl.dev>
[XSLT10]: <https://www.w3.org/TR/1999/REC-xslt-19991116>
