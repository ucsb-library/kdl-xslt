<!--
⁌ K·D·L → X·M·L Transformation (`KDL2XML.xslt´)


§ Usage

Apply this transform to a document with one or more <html:script> element with @type of `text/kdl´, `text/x-kdl´, or `text/x.kdl´, and the element will be replaced with an R·D·F/X·M·L representation of the contained K·D·L document.  As an alternative, an element with a @rdf:parseType of `KDL´ can be used to replace its contents with the R·D·F/X·M·L representation.  Alternatively, manually <call-template name="kdl2xml:parse"> with a `source´ param.

See <https://ns.1024.gdn/KDL/> for a description of the output format and vocabulary.


§ Overview

This transform provides a necessarily‐recursive parser for the K·D·L document language, which operates in two phases.

In the first phase (tokenization), the K·D·L document string is broken up into a series of tokens representing various language constructs; to achieve this task, the tokenizer must switch between a small handful of states (see <template name="kdl2xml:tokens">).  The output of tokenization is a series of elements with names beginning with ‹ kdl2xml:Token- ›.  If the input string cannot be tokenized, then at some point a <kdl2xml:TokenizationError> element will be output instead and further tokenization of the string will cease.

In the second phase (parsing), the flat tree of tokens is converted into the final result document.  Any errors in the tokenization phase, as well as any additional parse errors (correct tokens in wrong places; missing tokens), will result in a <kdl2xml:ParseError> element being output instead.

Readability aside, this provides a fairly straightforward parse strategy which is welsuited to X·S·L·T and probably trivial to port to other languages.  Note, however, that it does require a call stack size of a little greater than 𝑛 to process a K·D·L document containing 𝑛 tokens (without tail call optimizations).  It is recommended that K·D·L documents be broken up such that they only have one reasonably‐sized top·level node (with the source X·M·L document containing multiple such documents if necessary).


§ Configuration

Parse options can be set as a series of space‐separated values on the @data-⁠-k-d-l2-x-m-l-options attribute on the <html:script> element containing the K·D·L document.  The following options are supported :⁠—

•	`tokenize-only´: Skip the second stage of parsing and just output the string of parsed tokens (useful for debugging).


§ The kdl2xml Namespace

This file makes internal use of a number of R·D·F terms from the following namespace :⁠—

›	urn:fdc:dld.library.ucsb.edu:2024:kdl2xml:ns

The terms in this namespace described below.


¶ Classes

•	`kdl2xml:ParseError´
•	`kdl2xml:TokenizationError´

—⁠: These classes provide errors.  They have a `kdl2xml:sourceLocation´ providing the location of the error and an `rdfs:comment´ containing a description of the error.

•	`kdl2xml:Token-BinaryNumberPrefix´
•	`kdl2xml:Token-BinaryNumberDigits´
•	`kdl2xml:Token-DecimalNumberExponent´
•	`kdl2xml:Token-DecimalNumberFractionalDigits´
•	`kdl2xml:Token-DecimalNumberIntegralDigits´
•	`kdl2xml:Token-DecimalNumberPeriod´
•	`kdl2xml:Token-EOF´
•	`kdl2xml:Token-Equals´
•	`kdl2xml:Token-EscLineEnd´
•	`kdl2xml:Token-EscLineStart´
•	`kdl2xml:Token-HexNumberDigits´
•	`kdl2xml:Token-HexNumberPrefix´
•	`kdl2xml:Token-Identifier´
•	`kdl2xml:Token-Keyword´
•	`kdl2xml:Token-LineCommentContents´
•	`kdl2xml:Token-LineCommentStart´
•	`kdl2xml:Token-MultilineCommentEnd´
•	`kdl2xml:Token-MultilineCommentInternalCharacters´
•	`kdl2xml:Token-MultilineCommentStart´
•	`kdl2xml:Token-Newline´
•	`kdl2xml:Token-NodeChildrenEnd´
•	`kdl2xml:Token-NodeChildrenStart´
•	`kdl2xml:Token-NodeEnd´
•	`kdl2xml:Token-NodeStart´
•	`kdl2xml:Token-OctalNumberDigits´
•	`kdl2xml:Token-OctalNumberPrefix´
•	`kdl2xml:Token-QuotedStringEnd´
•	`kdl2xml:Token-QuotedStringInternalCharacters´
•	`kdl2xml:Token-QuotedStringInternalEscape´
•	`kdl2xml:Token-QuotedStringStart´
•	`kdl2xml:Token-RawStringEnd´
•	`kdl2xml:Token-RawStringInternals´
•	`kdl2xml:Token-RawStringStart´
•	`kdl2xml:Token-Sign´
•	`kdl2xml:Token-Slashdash´
•	`kdl2xml:Token-TypeAnnotationEnd´
•	`kdl2xml:Token-TypeAnnotationStart´
•	`kdl2xml:Token-Whitespace´

—⁠: These classes represent tokens.  They have a `kdl2xml:sourceLocation´ providing the location in the source at which the token begins and a `rdf:value´ containing the string value of the token.  `kdl2xml:Token-String´ is used for both quoted strings (of all varieties) and unquoted identifiers; look at the first few characters of its value to figure out which.  `kdl2xml:Token-Integer´ is used for all integers, regardless of base (again, check the first few characters).  Note that some tokens have a value of the empty string.

•	`kdl2xml:TokenizationResult´

—⁠: This class wraps some number of tokens and `kdl2xml:TokenizationError´s via its `kdl2xml:tokens´ property.  Its `kdl2xml:nextState´ property indicates the next state for the tokenizer.


¶ Object Properties

•	`kdl2xml:tokens´

—⁠: This property provides a list of tokens or `kdl2xml:TokenizationError`s.


¶ Data Properties

•	`kdl2xml:sourceLocation´

—⁠: This property provides the start location in the source for a token or error.

•	`kdl2xml:nextState´

—⁠: This property provides the next state for the tokenizer.


§ Copyright ⁊ Licence

›	Copyright © 2023 The Regents of the University of California.
›
›	Permission is hereby granted, free of charge, to any person obtaining a copy
›	of this software and associated documentation files (the "Software"), to deal
›	in the Software without restriction, including without limitation the rights
›	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
›	copies of the Software, and to permit persons to whom the Software is
›	furnished to do so, subject to the following conditions:
›
›	The above copyright notice and this permission notice shall be included in all
›	copies or substantial portions of the Software.
›
›	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
›	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
›	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
›	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
›	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
›	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
›	SOFTWARE.
-->
<!DOCTYPE transform [
	<!-- The X·S·D namespace is commonly used in specifying R·D·F datatypes, so it’s convenient to have an entity for it. -->
	<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>

	<!-- The following sets of characters are defined by K·D·L. -->
	<!ENTITY bom '&#xFEFF;'>
	<!--
	Due to a bug in libxml2 (<https://gitlab.gnome.org/GNOME/libxml2/-/issues/655>), it isn’t possible to include literal X·M·L whitespace in these entities in a way which will be correctly handled in attributes.  Instead, use the `att-´‐prefixed versions of these entities alongside the necessary literal `&#x9;´, `&#xA;´, `&#xD;´, and `&#x20;´ values.

	`&newline;´ = `&#xA;&#xD;&att-newline;´; `&whitespace;´ = `&#x9;&#x20;&att-whitespace;´; `&not-identifier;´ = `&#xA;&#xD;&#x9;&#x20;&att-not-identifier;´.

	<!ENTITY newline '&#x26;#xA;&#x26;#xD;&#x85;&#x2028;&#x2029;'>
	<!ENTITY whitespace '&#x26;#x9;&#x26;#x20;&#xA0;&#x1680;&#x2000;&#x2001;&#x2002;&#x2003;&#x2004;&#x2005;&#x2006;&#x2007;&#x2008;&#x2009;&#x200A;&#x202F;&#x205F;&#x3000;&bom;'>
	<!ENTITY not-identifier '&newline;&whitespace;\/(){}&lt;&gt;;[]=,&quot;'>
	-->
	<!ENTITY att-newline '&#x85;&#x2028;&#x2029;'> <!-- `&#xC;´ not allowed in XML 1.0; replace it with `&#xA;´ prior to applying this stylesheet -->
	<!ENTITY att-whitespace '&#xA0;&#x1680;&#x2000;&#x2001;&#x2002;&#x2003;&#x2004;&#x2005;&#x2006;&#x2007;&#x2008;&#x2009;&#x200A;&#x202F;&#x205F;&#x3000;&bom;'><!-- `&#xB;´ not allowed in XML 1.0; replace it with `&#x9;´ prior to applying this stylesheet -->
	<!ENTITY att-not-identifier '&att-newline;&att-whitespace;\/(){}&lt;&gt;;[]=,&quot;'>
	<!ENTITY digit '0123456789'>
	<!ENTITY sign '-+'>

	<!-- States are defined as entities to provide early errors on misspellings. -->
	<!ENTITY stateBeforeNode 'Before node'>
	<!ENTITY stateBeforeNodeNameOrTypeAnnotation 'Before node name or type annotation'>
	<!ENTITY stateAfterNodeSlashdash 'After node slashdash'>
	<!ENTITY stateAfterNodeTypeAnnotation 'After node type annotation'>
	<!ENTITY stateAfterNodeName 'After node name'>
	<!ENTITY stateAfterParameterSlashdash 'After parameter slashdash'>
	<!ENTITY stateBeforeParameterOrTypeAnnotation 'Before parameter or type annotation'>
	<!ENTITY stateAfterParameterTypeAnnotation 'After parameter type annotation'>
	<!ENTITY stateAfterParameterIdentifier 'After parameter identifier'>
	<!ENTITY stateBeforeParameterValue 'Before parameter value'>
	<!ENTITY stateAfterNodeChildren 'After node children'>

	<!-- The following are conveniences for certain X·S·L·T constructions.  Namespaces are provided again to appease `xmllint´. -->
	<!ENTITY withSource '<with-param xmlns="http://www.w3.org/1999/XSL/Transform" name="source" select="$source"/>'>
	<!ENTITY withPos '<with-param xmlns="http://www.w3.org/1999/XSL/Transform" name="pos" select="$pos"/>'>
	<!ENTITY sourceLocation '<kdl2xml:sourceLocation xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:kdl2xml="urn:fdc:dld.library.ucsb.edu:2024:kdl2xml:ns"><value-of select="$pos"/></kdl2xml:sourceLocation>'>
	<!ENTITY resolveMaybe '<variable xmlns="http://www.w3.org/1999/XSL/Transform"  xmlns:exsl="http://exslt.org/common" xmlns:kdl2xml="urn:fdc:dld.library.ucsb.edu:2024:kdl2xml:ns" name="result"><choose><when test="exsl:node-set($maybe)//kdl2xml:TokenizationError"/><otherwise><copy-of select="$maybe"/></otherwise></choose></variable>'>
	<!ENTITY forwardTokenizationErrorsFromRest '<when xmlns="http://www.w3.org/1999/XSL/Transform"  xmlns:exsl="http://exslt.org/common" xmlns:kdl2xml="urn:fdc:dld.library.ucsb.edu:2024:kdl2xml:ns" test="exsl:node-set($rest)//kdl2xml:TokenizationError"><copy-of select="exsl:node-set($rest)//kdl2xml:TokenizationError[1]"/></when>'>

	<!-- The following are fixed values for certain R·D·F properties on particular nodes. -->
	<!ATTLIST kdl2xml:sourceLocation
		rdf:datatype CDATA #FIXED "&xsd;integer">
	<!ATTLIST kdl2xml:tokens
		rdf:parseType CDATA #FIXED "Collection">
]>
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:exslstr="http://exslt.org/strings"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:kdl="https://ns.1024.gdn/KDL/#"
	xmlns:kdl2xml="urn:fdc:dld.library.ucsb.edu:2024:kdl2xml:ns"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
	exclude-result-prefixes="exsl exslstr"
	version="1.0"
>
	<!-- The following templates are called by the tokenizer to extract specific kinds of tokens. -->
	<template name="kdl2xml:tokens.binarydigits">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and contains('01_', $car)">
			<kdl2xml:Token-BinaryNumberDigits rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-BinaryNumberDigits>
			<call-template name="kdl2xml:tokens.binarydigits">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</if>
	</template>
	<template name="kdl2xml:tokens.decimaldigits">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and contains('&digit;_', $car)">
			<kdl2xml:Token-DecimalNumberIntegralDigits rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-DecimalNumberIntegralDigits>
			<call-template name="kdl2xml:tokens.decimaldigits">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</if>
	</template>
	<template name="kdl2xml:tokens.escline">
		<param name="source"/>
		<param name="pos"/>
		<choose>
			<when test="not(starts-with($source, '\'))">
				<kdl2xml:TokenizationError rdfs:comment="Token escline does not start with backslash">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<otherwise>
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.ws">
						<with-param name="source" select="substring($source, 2)"/>
						<with-param name="pos" select="number($pos)+1"/>
					</call-template>
				</variable>
				&resolveMaybe;
				<variable name="ws" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
				<variable name="car" select="substring($source, 2+string-length($ws), 1)"/>
				<variable name="cadr" select="substring($source, 3+string-length($ws), 1)"/>
				<variable name="rest">
					<choose>
						<when test="$car='&#x0D;' and $cadr='&#x0A;'">
							<kdl2xml:Token-Newline rdf:value="&#x0D;&#x0A;">
								<kdl2xml:sourceLocation>
									<value-of select="number($pos)+1+string-length($ws)"/>
								</kdl2xml:sourceLocation>
							</kdl2xml:Token-Newline>
						</when>
						<when test="contains('&#xA;&#xD;&att-newline;', $car)">
							<kdl2xml:Token-Newline rdf:value="{$car}">
								<kdl2xml:sourceLocation>
									<value-of select="number($pos)+1+string-length($ws)"/>
								</kdl2xml:sourceLocation>
							</kdl2xml:Token-Newline>
						</when>
						<when test="$car='/' and $cadr='/'">
							<call-template name="kdl2xml:tokens.linecomment">
								<with-param name="source" select="substring($source, 2+string-length($ws))"/>
								<with-param name="pos" select="number($pos)+1+string-length($ws)"/>
							</call-template>
						</when>
						<otherwise>
							<kdl2xml:TokenizationError rdfs:comment="Unexpected character while reading token escline">
								<kdl2xml:sourceLocation>
									<value-of select="number($pos)+1+string-length($ws)"/>
								</kdl2xml:sourceLocation>
							</kdl2xml:TokenizationError>
						</otherwise>
					</choose>
				</variable>
				<choose>
					&forwardTokenizationErrorsFromRest;
					<otherwise>
						<kdl2xml:Token-EscLineStart rdf:value="/">&sourceLocation;</kdl2xml:Token-EscLineStart>
						<copy-of select="$result"/>
						<copy-of select="$rest"/>
						<kdl2xml:Token-EscLineEnd rdf:value="">
							<kdl2xml:sourceLocation>
								<value-of select="number(pos)+1+string-length($ws)+string-length(exslstr:concat(exsl:node-set($rest)//@rdf:value))"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-EscLineEnd>
					</otherwise>
				</choose>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.exponent">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and contains('Ee', $car)">
			<variable name="cadr" select="substring($source, 2, 1)"/>
			<variable name="sign">
				<if test="contains('+-', $cadr)">
					<kdl2xml:Token-Sign rdf:value="{$cadr}">
						<kdl2xml:sourceLocation>
							<value-of select="number($pos)+1"/>
						</kdl2xml:sourceLocation>
					</kdl2xml:Token-Sign>
				</if>
			</variable>
			<variable name="signvalue" select="exslstr:concat(exsl:node-set($sign)//@rdf:value)"/>
			<variable name="maybe">
				<call-template name="kdl2xml:tokens.decimaldigits">
					<with-param name="source" select="substring($source, 2+string-length($signvalue))"/>
					<with-param name="pos" select="number($pos)+1+string-length($signvalue)"/>
				</call-template>
			</variable>
			&resolveMaybe;
			<variable name="integraldigits" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
			<choose>
				<when test="$integraldigits='' or starts-with($integraldigits, '_')"/>
				<otherwise>
					<kdl2xml:Token-DecimalNumberExponent rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-DecimalNumberExponent>
					<if test="$signvalue!=''">
						<copy-of select="$sign"/>
					</if>
					<kdl2xml:Token-DecimalNumberIntegralDigits rdf:value="{$integraldigits}">
						<kdl2xml:sourceLocation>
							<value-of select="number($pos)+1+string-length($signvalue)"/>
						</kdl2xml:sourceLocation>
					</kdl2xml:Token-DecimalNumberIntegralDigits>
				</otherwise>
			</choose>
		</if>
	</template>
	<template name="kdl2xml:tokens.hexdigits">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and contains('&digit;ABCDEFabcdef_', $car)">
			<kdl2xml:Token-HexNumberDigits rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-HexNumberDigits>
			<call-template name="kdl2xml:tokens.hexdigits">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</if>
	</template>
	<template name="kdl2xml:tokens.identifiertokens">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and not(contains('&#xA;&#xD;&#x9;&#x20;&att-not-identifier;', $car))">
			<kdl2xml:Token-Identifier rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Identifier>
			<call-template name="kdl2xml:tokens.identifiertokens">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</if>
	</template>
	<template name="kdl2xml:tokens.keyword">
		<param name="source"/>
		<param name="pos"/>
		<choose>
			<when test="starts-with($source, 'true')">
				<kdl2xml:Token-Keyword rdf:value="true">&sourceLocation;</kdl2xml:Token-Keyword>
			</when>
			<when test="starts-with($source, 'false')">
				<kdl2xml:Token-Keyword rdf:value="false">&sourceLocation;</kdl2xml:Token-Keyword>
			</when>
			<when test="starts-with($source, 'null')">
				<kdl2xml:Token-Keyword rdf:value="null">&sourceLocation;</kdl2xml:Token-Keyword>
			</when>
			<otherwise>
				<kdl2xml:TokenizationError rdfs:comment="Attempted to read token keyword, but no keyword was recognized">&sourceLocation;</kdl2xml:TokenizationError>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.linecomment">
		<param name="source"/>
		<param name="pos"/>
		<choose>
			<when test="not(starts-with($source, '//'))">
				<kdl2xml:TokenizationError rdfs:comment="Token linecomment does not begin with two slashes">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<otherwise>
				<variable name="thruend">
					<call-template name="kdl2xml:tokens.linecommentthruend">
						<with-param name="source" select="substring-after($source, '//')"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				<kdl2xml:Token-LineCommentStart rdf:value="//">&sourceLocation;</kdl2xml:Token-LineCommentStart>
				<kdl2xml:Token-LineCommentContents rdf:value="{exslstr:concat(exsl:node-set($thruend)//kdl2xml:Token-LineCommentContents/@rdf:value)}">
					<kdl2xml:sourceLocation>
						<value-of select="number($pos)+2"/>
					</kdl2xml:sourceLocation>
				</kdl2xml:Token-LineCommentContents>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.linecommentthruend">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="$car=''"/>
			<when test="$car='&#x0D;' and $cadr='&#x0A;'">
				<kdl2xml:Token-LineCommentContents rdf:value="&#x0D;&#x0A;">&sourceLocation;</kdl2xml:Token-LineCommentContents>
			</when>
			<when test="contains('&#xA;&#xD;&att-newline;', $car)">
				<kdl2xml:Token-LineCommentContents rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-LineCommentContents>
			</when>
			<otherwise>
				<kdl2xml:Token-LineCommentContents rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-LineCommentContents>
				<call-template name="kdl2xml:tokens.linecommentthruend">
					<with-param name="source" select="substring($source, 2)"/>
					<with-param name="pos" select="number($pos)+1"/>
				</call-template>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.multilinethruend">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="$car=''">
				<kdl2xml:TokenizationError rdfs:comment="Tokenization of multiline comment failed: Unexpected end of file">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<when test="$car='/' and $cadr='*'">
				<variable name="rest">
					<call-template name="kdl2xml:tokens.multilinethruend">
						<with-param name="source" select="substring($source, 3)"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				<variable name="stringvalue" select="exslstr:concat(exsl:node-set($rest)//@rdf:value)"/>
				<choose>
					&forwardTokenizationErrorsFromRest;
					<otherwise>
						<kdl2xml:Token-MultilineCommentStart rdf:value="/*">&sourceLocation;</kdl2xml:Token-MultilineCommentStart>
						<for-each select="exsl:node-set($rest)/*">
							<choose>
								<when test="self::kdl2xml:Token-MultilineCommentStart or self::kdl2xml:Token-MultilineCommentEnd">
									<copy-of select="."/>
								</when>
								<when test="self::kdl2xml:Token-MultilineCommentInternalCharacters[not(preceding-sibling::*[position()=1 and self::kdl2xml:Token-MultilineCommentInternalCharacters])]">
									<variable name="nextdelimiter" select="generate-id(following-sibling::kdl2xml:*[local-name()='Token-MultilineCommentStart' or local-name()='Token-MultilineCommentEnd'])"/>
									<kdl2xml:Token-MultilineCommentInternalCharacters rdf:value="{concat(@rdf:value, exslstr:concat(following-sibling::kdl2xml:Token-MultilineCommentInternalCharacters[not($nextdelimiter and preceding-sibling::*[generate-id()=$nextdelimiter])]/@rdf:value))}">
										<copy-of select="kdl2xml:sourceLocation"/>
									</kdl2xml:Token-MultilineCommentInternalCharacters>
								</when>
							</choose>
						</for-each>
						<call-template name="kdl2xml:tokens.multilinethruend">
							<with-param name="source" select="substring($source, 1+string-length($stringvalue))"/>
							<with-param name="pos" select="number($pos)+string-length($stringvalue)"/>
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$car='*' and $cadr='/'">
				<kdl2xml:Token-MultilineCommentEnd rdf:value="*/">&sourceLocation;</kdl2xml:Token-MultilineCommentEnd>
			</when>
			<otherwise>
				<kdl2xml:Token-MultilineCommentInternalCharacters rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-MultilineCommentInternalCharacters>
				<call-template name="kdl2xml:tokens.multilinethruend">
					<with-param name="source" select="substring($source, 2)"/>
					<with-param name="pos" select="number($pos)+1"/>
				</call-template>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.number">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="contains('+-', $car) and contains('+-', $cadr)">
				<kdl2xml:TokenizationError rdfs:comment="Number begins with multiple signs">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<when test="contains('+-', $car)">
				<kdl2xml:Token-Sign rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Sign>
				<call-template name="kdl2xml:tokens.number">
					<with-param name="source" select="substring($source, 2)"/>
					<with-param name="pos" select="number($pos)+1"/>
				</call-template>
			</when>
			<when test="$car='0' and $cadr='b'">
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.binarydigits">
						<with-param name="source" select="substring($source, 3)"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				&resolveMaybe;
				<variable name="binarydigits" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
				<choose>
					<when test="$binarydigits='' or starts-with($binarydigits, '_')">
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of number failed: Expected one or more binary digits">&sourceLocation;</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<kdl2xml:Token-BinaryNumberPrefix rdf:value="0b">&sourceLocation;</kdl2xml:Token-BinaryNumberPrefix>
						<kdl2xml:Token-BinaryNumberDigits rdf:value="{$binarydigits}">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+2"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-BinaryNumberDigits>
					</otherwise>
				</choose>
			</when>
			<when test="$car='0' and $cadr='x'">
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.hexdigits">
						<with-param name="source" select="substring($source, 3)"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				&resolveMaybe;
				<variable name="hexdigits" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
				<choose>
					<when test="$hexdigits='' or starts-with($hexdigits, '_')">
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of number failed: Expected one or more hex digits">&sourceLocation;</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<kdl2xml:Token-HexNumberPrefix rdf:value="0x">&sourceLocation;</kdl2xml:Token-HexNumberPrefix>
						<kdl2xml:Token-HexNumberDigits rdf:value="{$hexdigits}">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+2"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-HexNumberDigits>
					</otherwise>
				</choose>
			</when>
			<when test="$car='0' and $cadr='o'">
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.octaldigits">
						<with-param name="source" select="substring($source, 3)"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				&resolveMaybe;
				<variable name="octaldigits" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
				<choose>
					<when test="$octaldigits='' or starts-with($octaldigits, '_')">
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of number failed: Expected one or more octal digits">&sourceLocation;</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<kdl2xml:Token-OctalNumberPrefix rdf:value="0x">&sourceLocation;</kdl2xml:Token-OctalNumberPrefix>
						<kdl2xml:Token-OctalNumberDigits rdf:value="{$octaldigits}">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+2"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-OctalNumberDigits>
					</otherwise>
				</choose>
			</when>
			<otherwise>
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.decimaldigits">&withSource;&withPos;</call-template>
				</variable>
				&resolveMaybe;
				<variable name="integraldigits" select="exslstr:concat(exsl:node-set($result)//@rdf:value)"/>
				<choose>
					<when test="$integraldigits='' or starts-with($integraldigits, '_')">
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of number failed: Expected one or more integral digits">&sourceLocation;</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<variable name="afterresult" select="substring($source, 1+string-length($integraldigits), 1)"/>
						<kdl2xml:Token-DecimalNumberIntegralDigits rdf:value="{$integraldigits}">&sourceLocation;</kdl2xml:Token-DecimalNumberIntegralDigits>
						<choose>
							<when test="$afterresult='.'">
								<kdl2xml:Token-DecimalNumberPeriod rdf:value=".">
									<kdl2xml:sourceLocation>
										<value-of select="number($pos)+string-length($integraldigits)"/>
									</kdl2xml:sourceLocation>
								</kdl2xml:Token-DecimalNumberPeriod>
								<variable name="fraction">
									<call-template name="kdl2xml:tokens.decimaldigits">
										<with-param name="source" select="substring($source, 2+string-length($integraldigits))"/>
										<with-param name="pos" select="number($pos)+string-length($integraldigits)+1"/>
									</call-template>
								</variable>
								<variable name="fracdigits" select="exslstr:concat(exsl:node-set($fraction)//@rdf:value)"/>
								<if test="$fracdigits!='' or starts-with($fracdigits, '_')">
									<kdl2xml:Token-DecimalNumberFractionDigits rdf:value="{$fracdigits}">
										<kdl2xml:sourceLocation>
											<value-of select="number($pos)+string-length($integraldigits)+1"/>
										</kdl2xml:sourceLocation>
									</kdl2xml:Token-DecimalNumberFractionDigits>
								</if>
								<call-template name="kdl2xml:tokens.exponent">
									<with-param name="source" select="substring($source, 2+string-length($integraldigits)+string-length($fracdigits))"/>
									<with-param name="pos" select="number($pos)+string-length($integraldigits)+1+string-length($fracdigits)"/>
								</call-template>
							</when>
							<otherwise>
								<call-template name="kdl2xml:tokens.exponent">
									<with-param name="source" select="substring($source, 2+string-length($integraldigits))"/>
									<with-param name="pos" select="number($pos)+string-length($integraldigits)+1"/>
								</call-template>
							</otherwise>
						</choose>
					</otherwise>
				</choose>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.octaldigits">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<if test="$car and contains('01234567_', $car)">
			<kdl2xml:Token-OctalNumberDigits rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-OctalNumberDigits>
			<call-template name="kdl2xml:tokens.octaldigits">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</if>
	</template>
	<template name="kdl2xml:tokens.string">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<variable name="prequote">
			<if test="$car='r' and contains('#&quot;', $cadr)">
				<value-of select="substring-before($source, '&quot;')"/>
			</if>
		</variable>
		<variable name="open">
			<choose>
				<when test="substring($source, 1, 1)='&quot;'">&quot;</when>
				<when test="$prequote!='' and translate(substring($prequote, 2), '#', '')=''">
					<value-of select="$prequote"/>
					<text>&quot;</text>
				</when>
			</choose>
		</variable>
		<choose>
			<when test="$open='&quot;'">
				<variable name="rest">
					<call-template name="kdl2xml:tokens.stringinternals">
						<with-param name="source" select="substring-after($source, '&quot;')"/>
						<with-param name="pos" select="number($pos)+1"/>
					</call-template>
				</variable>
				<variable name="strvalue" select="exslstr:concat(exsl:node-set($rest)//@rdf:value)"/>
				<variable name="cloquot" select="substring($source, 2+string-length($strvalue), 1)"/>
				<choose>
					&forwardTokenizationErrorsFromRest;
					<when test="$cloquot!='&quot;'">
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of quoted string failed: Missing end of string">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+1+string-length($strvalue)"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<kdl2xml:Token-QuotedStringStart rdf:value="&quot;">&sourceLocation;</kdl2xml:Token-QuotedStringStart>
						<copy-of select="$rest"/>
						<kdl2xml:Token-QuotedStringEnd rdf:value="&quot;">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+1+string-length($strvalue)"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-QuotedStringEnd>
					</otherwise>
				</choose>
			</when>
			<when test="$open!=''">
				<variable name="hashes" select="substring($open, 2, string-length($open)-2)"/>
				<choose>
					<when test="contains($source, concat('&quot;', $hashes))">
						<variable name="contents" select="substring-before(substring-after($source, $open), concat('&quot;', $hashes))"/>
						<kdl2xml:Token-RawStringStart rdf:value="{$open}"/>
						<kdl2xml:Token-RawStringInternals rdf:value="{$contents}">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+string-length($open)"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-RawStringInternals>
						<kdl2xml:Token-RawStringEnd rdf:value="{concat('&quot;', $hashes)}">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+string-length($open)+string-length($contents)"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:Token-RawStringEnd>
					</when>
					<otherwise>
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of raw string failed: Missing end of string">&sourceLocation;</kdl2xml:TokenizationError>
					</otherwise>
				</choose>
			</when>
			<otherwise>
				<choose>
					<when test="not(contains('&#xA;&#xD;&#x9;&#x20;&att-not-identifier;&digit;&sign;', $car)) or contains('&sign;', $car) and not(contains('&#xA;&#xD;&#x9;&#x20;&att-not-identifier;&digit;', $cadr))">
						<variable name="rest">
							<call-template name="kdl2xml:tokens.identifiertokens">&withSource;&withPos;</call-template>
						</variable>
						<variable name="identifier" select="exslstr:concat(exsl:node-set($rest)//@rdf:value)"/>
						<choose>
							&forwardTokenizationErrorsFromRest;
							<when test="$identifier='' or $identifier='true' or $identifier='false' or $identifier='null'">
								<kdl2xml:TokenizationError rdfs:comment="Tokenization of identifier failed: Identifier was empty or matched a known keyword">&sourceLocation;</kdl2xml:TokenizationError>
							</when>
							<otherwise>
								<kdl2xml:Token-Identifier rdf:value="{$identifier}">&sourceLocation;</kdl2xml:Token-Identifier>
							</otherwise>
						</choose>
					</when>
					<otherwise>
						<kdl2xml:TokenizationError rdfs:comment="Tokenization of identifier failed: Identifier begins with an invalid character">&sourceLocation;</kdl2xml:TokenizationError>
					</otherwise>
				</choose>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.stringinternals">
		<param name="source"/>
		<param name="pos"/>
		<variable name="rest">
			<call-template name="kdl2xml:tokens.stringinternaltokens">&withSource;&withPos;</call-template>
		</variable>
		<choose>
			&forwardTokenizationErrorsFromRest;
			<otherwise>
				<for-each select="exsl:node-set($rest)/*">
					<choose>
						<when test="self::kdl2xml:Token-QuotedStringInternalEscape">
							<copy-of select="."/>
						</when>
						<when test="self::kdl2xml:Token-QuotedStringInternalCharacters[not(preceding-sibling::*[position()=1 and self::kdl2xml:Token-QuotedStringInternalCharacters])]">
							<variable name="nextescape" select="generate-id(following-sibling::kdl2xml:Token-QuotedStringInternalEscape)"/>
							<kdl2xml:Token-QuotedStringInternalCharacters rdf:value="{concat(@rdf:value, exslstr:concat(following-sibling::kdl2xml:Token-QuotedStringInternalCharacters[not($nextescape and preceding-sibling::*[generate-id()=$nextescape])]/@rdf:value))}">
								<copy-of select="kdl2xml:sourceLocation"/>
							</kdl2xml:Token-QuotedStringInternalCharacters>
						</when>
					</choose>
				</for-each>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.stringinternaltokens">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="$car=''">
				<kdl2xml:TokenizationError rdfs:comment="Unexpected end of file inside of quoted string">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<when test="$car='&quot;'"/>
			<when test="$car='\' and contains('nrt\/&quot;bf', $cadr)">
				<kdl2xml:Token-QuotedStringInternalEscape rdf:value="{concat($car, $cadr)}">&sourceLocation;</kdl2xml:Token-QuotedStringInternalEscape>
				<call-template name="kdl2xml:tokens.stringinternaltokens">
					<with-param name="source" select="substring($source, 3)"/>
					<with-param name="pos" select="number($pos)+2"/>
				</call-template>
			</when>
			<when test="starts-with($source, '\u{')">
				<variable name="codepoint" select="substring-before(substring-after($source, '\u{'), '}')"/>
				<choose>
					<when test="not($codepoint) or string-length($codepoint)>6 or string-length($codepoint)>5 and not(starts-with($codepoint, '10')) or translate($codepoint, '&digit;ABCDEFabcdef', '')">
						<kdl2xml:TokenizationError rdfs:comment="Invalid unicode escape sequence inside quoted string">
							<kdl2xml:sourceLocation>
								<value-of select="number($pos)+2"/>
							</kdl2xml:sourceLocation>
						</kdl2xml:TokenizationError>
					</when>
					<otherwise>
						<kdl2xml:Token-QuotedStringInternalEscape rdf:value="{concat('\u{', $codepoint, '}')}">&sourceLocation;</kdl2xml:Token-QuotedStringInternalEscape>
						<call-template name="kdl2xml:tokens.stringinternaltokens">
							<with-param name="source" select="substring($source, 5+string-length($codepoint))"/>
							<with-param name="pos" select="number($pos)+4+string-length($codepoint)"/>
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$car='\'">
				<kdl2xml:TokenizationError rdfs:comment="Invalid escape sequence inside of quoted string">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<otherwise>
				<kdl2xml:Token-QuotedStringInternalCharacters rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-QuotedStringInternalCharacters>
				<call-template name="kdl2xml:tokens.stringinternaltokens">
					<with-param name="source" select="substring($source, 2)"/>
					<with-param name="pos" select="number($pos)+1"/>
				</call-template>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.typeannotation">
		<param name="source"/>
		<param name="pos"/>
		<variable name="rest">
			<call-template name="kdl2xml:tokens.string">
				<with-param name="source" select="substring($source, 2)"/>
				<with-param name="pos" select="number($pos)+1"/>
			</call-template>
		</variable>
		<variable name="ident" select="exslstr:concat(exsl:node-set($rest)//@rdf:value)"/>
		<variable name="cloparen" select="substring($source, 2+string-length($ident), 1)"/>
		<choose>
			&forwardTokenizationErrorsFromRest;
			<when test="substring($source, 1, 1)!='('">
				<kdl2xml:TokenizationError rdfs:comment="Tokenization of type annotation failed: Missing opening parenthesis">&sourceLocation;</kdl2xml:TokenizationError>
			</when>
			<when test="$cloparen!=')'">
				<kdl2xml:TokenizationError rdfs:comment="Tokenization of type annotation failed: Missing closing parenthesis">
					<kdl2xml:sourceLocation>
						<value-of select="number($pos)+1+string-length($ident)"/>
					</kdl2xml:sourceLocation>
				</kdl2xml:TokenizationError>
			</when>
			<otherwise>
				<kdl2xml:Token-TypeAnnotationStart rdf:value="(">&sourceLocation;</kdl2xml:Token-TypeAnnotationStart>
				<copy-of select="$rest"/>
				<kdl2xml:Token-TypeAnnotationEnd rdf:value=")">
					<kdl2xml:sourceLocation>
						<value-of select="number($pos)+1+string-length($ident)"/>
					</kdl2xml:sourceLocation>
				</kdl2xml:Token-TypeAnnotationEnd>
			</otherwise>
		</choose>
	</template>
	<template name="kdl2xml:tokens.ws">
		<param name="source"/>
		<param name="pos"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="$car and contains('&#x9;&#x20;&att-whitespace;', $car)">
				<variable name="maybe">
					<call-template name="kdl2xml:tokens.ws">
						<with-param name="source" select="substring($source, 2)"/>
						<with-param name="pos" select="number($pos)+1"/>
					</call-template>
				</variable>
				&resolveMaybe;
				<choose>
					<when test="$result and exsl:node-set($result)/*[position()=1 and self::kdl2xml:Token-Whitespace]">
						<kdl2xml:Token-Whitespace rdf:value="{concat($car, exsl:node-set($result)/*[1]/@rdf:value)}">&sourceLocation;</kdl2xml:Token-Whitespace>
						<copy-of select="exsl:node-set($result)/*[position()!=1]"/>
					</when>
					<otherwise>
						<kdl2xml:Token-Whitespace rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Whitespace>
						<if test="$result">
							<copy-of select="$result"/>
						</if>
					</otherwise>
				</choose>
			</when>
			<when test="$car='/' and $cadr='*'">
				<variable name="rest">
					<call-template name="kdl2xml:tokens.multilinethruend">
						<with-param name="source" select="substring($source, 3)"/>
						<with-param name="pos" select="number($pos)+2"/>
					</call-template>
				</variable>
				<variable name="stringvalue" select="exslstr:concat(exsl:node-set($rest)//@rdf:value)"/>
				<choose>
					&forwardTokenizationErrorsFromRest;
					<otherwise>
						<variable name="maybe">
							<call-template name="kdl2xml:tokens.ws">
								<with-param name="source" select="substring($source, 3+string-length($stringvalue))"/>
								<with-param name="pos" select="number($pos)+2+string-length($stringvalue)"/>
							</call-template>
						</variable>
						&resolveMaybe;
						<kdl2xml:Token-MultilineCommentStart rdf:value="/*">&sourceLocation;</kdl2xml:Token-MultilineCommentStart>
						<for-each select="exsl:node-set($rest)/*">
							<choose>
								<when test="self::kdl2xml:Token-MultilineCommentStart or self::kdl2xml:Token-MultilineCommentEnd">
									<copy-of select="."/>
								</when>
								<when test="self::kdl2xml:Token-MultilineCommentInternalCharacters[not(preceding-sibling::*[position()=1 and self::kdl2xml:Token-MultilineCommentInternalCharacters])]">
									<variable name="nextdelimiter" select="generate-id(following-sibling::kdl2xml:*[local-name()='Token-MultilineCommentStart' or local-name()='Token-MultilineCommentEnd'])"/>
									<kdl2xml:Token-MultilineCommentInternalCharacters rdf:value="{concat(@rdf:value, exslstr:concat(following-sibling::kdl2xml:Token-MultilineCommentInternalCharacters[not($nextdelimiter and preceding-sibling::*[generate-id()=$nextdelimiter])]/@rdf:value))}">
										<copy-of select="kdl2xml:sourceLocation"/>
									</kdl2xml:Token-MultilineCommentInternalCharacters>
								</when>
							</choose>
						</for-each>
						<if test="$result">
							<copy-of select="$result"/>
						</if>
					</otherwise>
				</choose>
			</when>
			<otherwise>
				<kdl2xml:TokenizationError rdfs:comment="Invalid character in whitespace">&sourceLocation;</kdl2xml:TokenizationError>
			</otherwise>
		</choose>
	</template>

	<!-- The following template returns a <kdl2xml:TokenizationResult> indicating the next token(s) from the provided string. -->
	<template name="kdl2xml:tokens">
		<param name="source"/>
		<param name="state" select="'Before node'"/>
		<param name="pos" select="1"/>
		<variable name="car" select="substring($source, 1, 1)"/>
		<variable name="cadr" select="substring($source, 2, 1)"/>
		<choose>
			<when test="$car=''">
				<!-- The source has been exhausted. -->
				<kdl2xml:TokenizationResult>
					<kdl2xml:tokens>
						<if test="$state='&stateAfterNodeChildren;' or $state='&stateAfterNodeName;'">
							<kdl2xml:Token-NodeEnd>&sourceLocation;</kdl2xml:Token-NodeEnd>
						</if>
						<kdl2xml:Token-EOF>&sourceLocation;</kdl2xml:Token-EOF>
					</kdl2xml:tokens>
				</kdl2xml:TokenizationResult>
			</when>
			<when test="$state='&stateBeforeNode;'">
				<!-- The parser is in this state at the beginning of the document and in‐between nodes. -->
				<choose>
					<when test="$car='&#x0D;' and $cadr='&#x0A;'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Newline rdf:value="&#x0D;&#x0A;">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('&#xA;&#xD;&att-newline;', $car)">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Newline rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='/' and $cadr='/'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.linecomment">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('&#x9;&#x20;&att-whitespace;', $car) or $car='/' and $cadr='*'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.ws">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='/' and $cadr='-'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeSlashdash;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Slashdash rdf:value="/-">&sourceLocation;</kdl2xml:Token-Slashdash>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='}'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeChildren;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeChildrenEnd rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-NodeChildrenEnd>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateBeforeNodeNameOrTypeAnnotation;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateAfterNodeSlashdash;'">
				<!-- The parser is in this state after reading a slashdash in a place where nodes are expected. -->
				<choose>
					<when test="contains('&#x9;&#x20;&att-whitespace;', $car) or $car='/' and $cadr='*'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeSlashdash;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.ws">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='\'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeSlashdash;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.escline">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateBeforeNodeNameOrTypeAnnotation;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateBeforeNodeNameOrTypeAnnotation;'">
				<!-- The parser is in this state after exhausting all whitespace before a node. -->
				<choose>
					<when test="$car='('">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeTypeAnnotation;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeStart rdf:value="">&sourceLocation;</kdl2xml:Token-NodeStart>
								<call-template name="kdl2xml:tokens.typeannotation">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeStart rdf:value="">&sourceLocation;</kdl2xml:Token-NodeStart>
								<call-template name="kdl2xml:tokens.string">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateAfterNodeTypeAnnotation;'">
				<!-- The parser is in this state after reading a type annotation in a place where nodes are expected. -->
				<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
					<kdl2xml:tokens>
						<call-template name="kdl2xml:tokens.string">&withSource;&withPos;</call-template>
					</kdl2xml:tokens>
				</kdl2xml:TokenizationResult>
			</when>
			<when test="$state='&stateAfterNodeName;'">
				<!-- The parser is in this state after reading an identifier in a place where nodes are expected. -->
				<choose>
					<when test="$car='&#x0D;' and $cadr='&#x0A;'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<kdl2xml:Token-Newline rdf:value="&#x0D;&#x0A;">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('&#xA;&#xD;&att-newline;', $car)">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<kdl2xml:Token-Newline rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='/' and $cadr='/'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<call-template name="kdl2xml:tokens.linecomment">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car=';'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value=";">&sourceLocation;</kdl2xml:Token-NodeEnd>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('&#x9;&#x20;&att-whitespace;', $car) or $car='/' and $cadr='*'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.ws">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='\'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.escline">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='/' and $cadr='-'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterSlashdash;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Slashdash rdf:value="/-">&sourceLocation;</kdl2xml:Token-Slashdash>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='{'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeChildrenStart rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-NodeChildrenStart>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateBeforeParameterOrTypeAnnotation;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateAfterParameterSlashdash;'">
				<!-- The parser is in this state after reading a slashdash in a place where parameters are expected. -->
				<choose>
					<when test="contains('&#x9;&#x20;&att-whitespace;', $car) or $car='/' and $cadr='*'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterSlashdash;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.ws">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='\'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterSlashdash;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.escline">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='{'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeChildrenStart rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-NodeChildrenStart>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateBeforeParameterOrTypeAnnotation;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateBeforeParameterOrTypeAnnotation;'">
				<!-- The parser is in this state after exhausting all whitespace before a parameter. -->
				<choose>
					<when test="$car='('">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterTypeAnnotation;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.typeannotation">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='&quot;' or not(contains('&#xA;&#xD;&#x9;&#x20;&att-not-identifier;&sign;&digit;', $car)) or contains('&sign;', $car) and not(contains('&#xA;&#xD;&#x9;&#x20;&att-not-identifier;&digit;', $cadr))">
						<!-- Strings which occur here might be introducing properties, so they need to result in a different parse state. -->
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterIdentifier;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.string">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateBeforeParameterValue;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateAfterParameterTypeAnnotation;'">
				<!-- The parser is in this state after reading a type annotation in a place where parameter values are expected. -->
				<call-template name="kdl2xml:tokens">
					&withSource;
					<with-param name="state" select="'&stateBeforeParameterValue;'"/>
					&withPos;
				</call-template>
			</when>
			<when test="$state='&stateAfterParameterIdentifier;'">
				<!-- The parser is in this state after reading an identifier which could be either a property name or an argument value. -->
				<choose>
					<when test="$car='=' and $cadr='('">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterParameterTypeAnnotation;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Equals rdf:value="=">&sourceLocation;</kdl2xml:Token-Equals>
								<call-template name="kdl2xml:tokens.typeannotation">
									<with-param name="source" select="substring($source, 2)"/>
									<with-param name="pos" select="number($pos)+1"/>
								</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='='">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeParameterValue;">
							<kdl2xml:tokens>
								<kdl2xml:Token-Equals rdf:value="=">&sourceLocation;</kdl2xml:Token-Equals>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<call-template name="kdl2xml:tokens">
							&withSource;
							<with-param name="state" select="'&stateAfterNodeName;'"/>
							&withPos;
						</call-template>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateBeforeParameterValue;'">
				<!-- The parser is in this state when a parameter value is immediately expected. -->
				<choose>
					<when test="contains('tfn', $car)">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.keyword">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('0123456789', $car) or contains('-+', $car) and contains('0123456789', $cadr)">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.number">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateAfterNodeName;">
							<kdl2xml:tokens>
								<call-template name="kdl2xml:tokens.string">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</otherwise>
				</choose>
			</when>
			<when test="$state='&stateAfterNodeChildren;'">
				<!-- The parser is in this state when the children block of a node has closed. -->
				<choose>
					<when test="$car='&#x0D;' and $cadr='&#x0A;'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<kdl2xml:Token-Newline rdf:value="&#x0D;&#x0A;">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="contains('&#xA;&#xD;&att-newline;', $car)">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<kdl2xml:Token-Newline rdf:value="{$car}">&sourceLocation;</kdl2xml:Token-Newline>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car='/' and $cadr='/'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value="">&sourceLocation;</kdl2xml:Token-NodeEnd>
								<call-template name="kdl2xml:tokens.linecomment">&withSource;&withPos;</call-template>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<when test="$car=';'">
						<kdl2xml:TokenizationResult kdl2xml:nextState="&stateBeforeNode;">
							<kdl2xml:tokens>
								<kdl2xml:Token-NodeEnd rdf:value=";">&sourceLocation;</kdl2xml:Token-NodeEnd>
							</kdl2xml:tokens>
						</kdl2xml:TokenizationResult>
					</when>
					<otherwise>
						<kdl2xml:TokenizationError rdfs:comment="Unexpected character after node children">&sourceLocation;</kdl2xml:TokenizationError>
					</otherwise>
				</choose>
			</when>
		</choose>
	</template>

	<template name="kdl2xml:tokenize">
		<param name="source"/>
		<param name="state" select="'Before node'"/>
		<param name="pos" select="1"/>
		<variable name="next">
			<call-template name="kdl2xml:tokens">
				&withSource;
				<with-param name="state" select="$state"/>
				&withPos;
			</call-template>
		</variable>
		<variable name="consumed" select="exslstr:concat(exsl:node-set($next)//kdl2xml:tokens/*/@rdf:value)"/>
		<variable name="nextState" select="string(exsl:node-set($next)//kdl2xml:TokenizationResult[last()]/@kdl2xml:nextState)"/>
		<choose>
			<when test="exsl:node-set($next)//kdl2xml:TokenizationError">
				<copy-of select="exsl:node-set($next)//kdl2xml:TokenizationError[1]"/>
			</when>
			<when test="not($consumed) and $state=$nextState">
				<kdl2xml:TokenizationError>
					<attribute name="rdfs:comment">
						<text>Tokenization consumed zero characters in state </text>
						<value-of select="$state"/>
					</attribute>
					&sourceLocation;
				</kdl2xml:TokenizationError>
			</when>
			<otherwise>
				<copy-of select="exsl:node-set($next)//kdl2xml:TokenizationResult/kdl2xml:tokens/*"/>
				<if test="$nextState!=''">
					<call-template name="kdl2xml:tokenize">
						<with-param name="source" select="substring($source, 1+string-length($consumed))"/>
						<with-param name="state" select="$nextState"/>
						<with-param name="pos" select="$pos+string-length($consumed)"/>
					</call-template>
				</if>
			</otherwise>
		</choose>
	</template>

	<template name="kdl2xml:tokens2nodes">
		<param name="tokenized" select="/.."/>
		<kdl2xml:ParseError rdfs:comment="Not implemented yet!"/>
	</template>

	<template name="kdl2xml:parse">
		<param name="source"/>
		<variable name="tokenized">
			<call-template name="kdl2xml:tokenize">&withSource;</call-template>
		</variable>
		<choose>
			<when test="exsl:node-set($tokenized)//kdl2xml:TokenizationError">
				<copy-of select="exsl:node-set($tokenized)//kdl2xml:TokenizationError[1]"/>
			</when>
			<otherwise>
				<variable name="document">
					<call-template name="kdl2xml:tokens2nodes">
						<with-param name="tokens" select="exsl:node-set($tokenized)"/>
					</call-template>
				</variable>
				<choose>
					<when test="exsl:node-set($document)//kdl2xml:ParseError">
						<copy-of select="exsl:node-set($document)//kdl2xml:ParseError[1]"/>
					</when>
					<otherwise>
						<copy-of select="$document"/>
					</otherwise>
				</choose>
			</otherwise>
		</choose>
	</template>

	<template match="*" mode="kdl2xml:parseelementcontents">
		<choose>
			<when test="contains(concat(' ', normalize-space(@data--k-d-l2-x-m-l-options), ' '), ' tokenize-only ')">
				<kdl2xml:TokenizationResult>
					<kdl2xml:tokens>
						<call-template name="kdl2xml:tokenize">
							<with-param name="source" select="string(.)"/>
						</call-template>
					</kdl2xml:tokens>
				</kdl2xml:TokenizationResult>
			</when>
			<otherwise>
				<call-template name="kdl2xml:parse">
					<with-param name="source" select="string(.)"/>
				</call-template>
			</otherwise>
		</choose>
	</template>

	<template match="html:script[@type='text/kdl']">
		<apply-templates mode="kdl2xml:parseelementcontents" select="."/>
	</template>
	<template match="html:script[@type='text/x.kdl']">
		<apply-templates mode="kdl2xml:parseelementcontents" select="."/>
	</template>
	<template match="html:script[@type='text/x-kdl']">
		<apply-templates mode="kdl2xml:parseelementcontents" select="."/>
	</template>
	<template match="*[@rdf:parseType='KDL']">
		<copy>
			<for-each select="@*[not(self::rdf:parseType)]">
				<copy/>
			</for-each>
			<call-template name="kdl2xml:parse">
				<with-param name="source" select="string(.)"/>
			</call-template>
		</copy>
	</template>
	<template match="*[@rdf:parseType='KDL-Tokens']">
		<copy>
			<for-each select="@*[not(self::rdf:parseType)]">
				<copy/>
			</for-each>
			<kdl2xml:TokenizationResult>
				<kdl2xml:tokens>
					<call-template name="kdl2xml:tokenize">
						<with-param name="source" select="string(.)"/>
					</call-template>
				</kdl2xml:tokens>
			</kdl2xml:TokenizationResult>
		</copy>
	</template>
</transform>
