SHELL = /bin/sh

# A simple Makefile for performing common tasks with `KDL2XML.xslt´.

KDL2XML = KDL2XML.xslt
SOURCE = example.kdl
OUTPUT = $(basename $(SOURCE)).xml
XMLLINTOPTS = --noout

# Call with a source file as the first argument, and, optionally, parse options as the second.
#
# Characters which are allowed in K·D·L but not in X·M·L must be sanitized from the input; replace vertical tab → horizontal tab, form feed → newline, and {all other controls, U+FFFE & U+FFFF} → the replacement character.
#
# Also perform X·M·L 1.1 line·break normalization, because K·D·L really should be doing this.
#
# (The following `sed´ script cannot tell if the input ends with a line·feed and assumes that it does not; this may result in a harmless extra newline if it does.)
override processkdl = printf '%s\n' "$$(printf '%b' '<?xml version=\042\061.0\042?>\n<script xmlns=\0042http://www.w3.org/1999/xhtml\0042 type=\0042text/kdl\0042$(if $2, data--k-d-l2-x-m-l-options=\0042$2\0042,)><![CDATA['; cat $1 | tr '\000\013\014' '\032\011\012' | sed $$(printf '%s%b' 's/]]>/]]]]><!\[CDATA\[>/g;s/\xEF\xBF\xBE/�/g;s/\xEF\xBF\xBF/�/g;$$!s/\r$$//g;s/\r/\n/g;$$!s/\xC2\x85$$//g;s/\xC2\x85/\n/g;s/\xE2\x80\xA8/\n/g;' 's/[\0001-\0010]/�/g;s/[\0016-\0037]/�/g'); printf '%s' ']]></script>')" | xsltproc KDL2XML.xslt -

help:
	@echo ' ╭──────────╮'
	@echo '╔╡ [1mKDL~XSLT[22m ╞════════════════════════════════════════════════════════╗'
	@echo '║╰──────────╯                                                        ║'
	@echo '║ [1mFor developers :—[22m                                                 ║'
	@echo '║ • [40m[91mmake[39m [94mlint[39m[49m: Lint `KDL2XML.xslt´                                   ║'
	@echo '║ • [40m[31mmake[39m [94mtokenize[39m [92mSOURCE[39m=[96mmy.kdl[39m[49m – Tokenize `my.kdl´ (for debugging)  ║'
	@echo '║                                                                    ║'
	@echo '║ [1mFor users :—[22m                                                      ║'
	@echo '║ • [40m[31mmake[39m [94mkdl2xml[39m [92mSOURCE[39m=[96mmy.kdl[39m[49m – Convert `my.kdl´ to X·M·L           ║'
	@echo '╚════════════════════════════════════════════════════════════════════╝'

# `make lint´ lints `KDL2XML.xslt´ using `xmllint´ to ensure basic X·M·L
# welformedness.
lint:
	xmllint $(XMLLINTOPTS) $(KDL2XML)

# `make kdl2xml SOURCE=my-document.kdl´ will parse a K·D·L file to X·M·L.
kdl2xml:
	$(call processkdl,$(SOURCE),)$(if $(filter -,$(OUTPUT)),, > $(OUTPUT))

# `make tokenize SOURCE=my-document.kdl´ will tokenize a K·D·L file to X·M·L.
tokenize:
	$(call processkdl,$(SOURCE),tokenize-only)$(if $(filter -,$(OUTPUT)),, > $(OUTPUT))

.PHONY: kdl2xml lint tokenize;
